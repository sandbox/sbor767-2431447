<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for Bootstrap based themes when admin theme is not.
 *
 * Date: 22.02.2015
 * Time: 18:51
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function lazybootstrap_form_system_theme_settings_alter(&$form, $form_state, $form_id = NULL) {
  $form['lazybootstrap_content_only'] = array(
    '#type' => 'checkbox',
    '#title' => t('Content only.'),
    '#description' => t('Hide all in the template except for content.'),
    '#default_value' => theme_get_setting('lazybootstrap_content_only'),
    '#weight' => -10,
  );

}
