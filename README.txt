
Lazy Bootstrap, chield theme from 'bootstrap' theme.

This lesson7 theme practics for training by Konstantin Komelin,
 http://morningcurve.com to study the Drupal system.

Your can see changes in:
 'theme-settings.php' - hook lazybootstrap_form_system_theme_settings_alter
    for define theme variable 'lazybootstrap_content_only' displayed as checkbox.
 'lazybootstrap.info' - for set default value 'lazybootstrap_content_only'.
 'template.php' - hook lazybootstrap_preprocess_page for transfer
   value 'lazybootstrap_content_only' through &$variables to page.tpl.php .
 'page.tpl.php' - changed template for hide all regions except region 'content'
  if variable 'lazybootstrap_only_content' set.
