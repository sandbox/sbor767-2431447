<?php

/**
 * @file
 * template.php
 */

/**
 * Preprocess page function.
 *
 * Set variable for access it from page.tpl.php for
 * hide all regions other then the 'content' region.
 */
function lazybootstrap_preprocess_page(&$variables) {

  $variables['lazybootstrap_only_content'] = theme_get_setting('lazybootstrap_content_only');

}
